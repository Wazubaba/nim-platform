#[
  This module complements Nim's existing os abstraction features whilst
  attempting to offer proper support to all platforms that the standard lib's
  OS module can detect, as specified within the documentation.
]#

import os
import strformat
when defined(Linux): import xdg

#[
  https://stackoverflow.com/questions/43853548/xdg-basedir-directories-for-windows
  $XDG_DATA_HOME = %LOCALAPPDATA%
  $XDG_DATA_DIRS = %APPDATA%
  $XDG_CONFIG_HOME = %LOCALAPPDATA%
  $XDG_CONFIG_DIRS = %APPDATA%
  $XDG_CACHE_HOME = %TEMP%
  $XDG_RUNTIME_DIR = %TEMP%

]#


proc resolveConfigDir*: string =
  when defined(Linux): return xdg_config_home()
  elif defined(Windows): return getEnv("LOCALAPPDATA")
  elif defined Osx:
    let user = getEnv("USER")
    if user.len == 0: raise newException(OSError, "Failed to read current user")
    return &"/Users/{user}/Library/Preferences"
  elif defined Bsd:
    return getConfigDir()# TODO: figure this out...
  else: return getAppDir() # TODO: Find a better way to handle this...


proc resolveSystemConfigDir*: string =
  ## Get the system-level configuration directory
  when defined(Linux): return "/etc"
  elif defined(Windows): return getEnv("APPDATA")
  elif defined(Bsd): return "/usr/local/etc" # TODO: Confirm?
  elif defined(Osx): return "/Library/Preferences" # TODO: Confirm?
  else: return getAppDir() # I fucking hate the term "app"... I don't
                           # write apps. I write software you fucking
                           # phone-toting mouthbreathers.. also
                           # TODO: Find a better way

proc resolveTempDir*: string =
  when defined(Linux) or defined(Bsd): return "/tmp"
  elif defined(Windows): return getEnv("TEMP")
  elif defined(Osx): return "/tmp" # TODO: Deterine...
  else: return getAppDir() # Every time I write this I want to scream. TODO^

proc resolveHomeDir*: string =
  when defined(Linux) or defined(Bsd) or defined(Osx): return $getEnv("HOME")
  elif defined(Windows): return $getEnv("HOMEPATH")
  else: return getAppDir() # ARGH also TODO still need to come up with a
                           # better way...

proc resolveIncludeDir*: string =
  ## Get the platform include directory if available
  when defined(Linux) or defined(Bsd): return "/usr/include"
  elif defined(Windows): return getAppDir() # TODO: I think this is right?
  elif defined(Osx): raise newException(NotYetImplementedError, "IDK wtf osx puts these")
  else: raise newException(NotYetImplementedError, "I have no fucking idea :|")

proc resolveLibraryDir*: string =
  ## Get the platform library directory if available
  when defined(Linux) or defined(Bsd): return "/usr/lib"
  elif defined(Windows): return getAppDir() # TODO: Verify
  elif defined(Osx): raise newException(NotYetImplementedError, "IDK wtf osx puts these")
  else: raise newException(NotYetImplementedError, "I have no fucking idea :|")

proc get_binary_directory*: string =
  ## Get the platform binary directory if available
  when defined(Linux) or defined(Bsd): return "/usr/bin"
  elif defined(Windows): return getAppDir() # TODO: Verify
  elif defined(Osx): raise newException(NotYetImplementedError, "IDK wtf osx puts these")
  else: raise newException(NotYetImplementedError, "I have no fucking idea :|")

proc get_cache_directory*: string =
  ## Get the platform cache directory if available
  when defined(Linux): return xdg_cache_home()
  elif defined(Windows): return getEnv("TEMP")
  elif defined(Osx): raise newException(NotYetImplementedError, "IDK wtf osx puts these")
  else: raise newException(NotYetImplementedError, "I have no fucking idea :|")
