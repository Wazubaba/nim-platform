import os
import strutils

## Each function is named after the variable it resolves, so if you
## want further documentation see
## https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html

## Any function that is supposed to return a colon-separated string returns a
## sequence of strings instead.

proc xdg_data_home*: string =
  result = getEnv("XDG_DATA_HOME")
  if result.len == 0:
    result = getEnv("HOME")/".local"/"share"

proc xdg_data_dirs*: seq[string] =
  let buffer = getEnv("XDG_DATA_DIRS")
  if buffer.len == 0:
    result = buffer.split(":")
  else:
    result = @["/usr"/"local"/"share/","/usr"/"share/"]

proc xdg_config_home*: string =
  result = getEnv("XDG_CONFIG_HOME")
  if result.len == 0:
    result = getEnv("HOME")/".config"

proc xdg_config_dirs*: seq[string] =
  let buffer = getEnv("XDG_CONFIG_DIRS")
  if buffer.len == 0:
    result = buffer.split(":")
  else:
    result = @["/etc"/"xdg"]

proc xdg_cache_home*: string =
  result = getEnv("XDG_CACHE_HOME")
  if result.len == 0:
    result = getEnv("HOME")/".cache"

proc xdg_runtime_dir*: string =
  result = getEnv("XDG_RUNTIME_DIR")
  if result.len == 0:
    result = "/tmp"

