import nake
import os
import strformat

task "default", "":
  let src = "tests"/"src"/"test.nim"
  let dst = "tests"/"bin"/"test".changeFileExt(ExeExt)
  createdir("test"/"bin")

  direShell(nimExe, "c", "-r", &"--out:{dst}", src)

task "clean", "":
  removeDir("tests"/"bin")
